package advprog.example.bot;

import com.linecorp.bot.model.event.Event;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.message.TextMessage;
import com.linecorp.bot.spring.boot.annotation.EventMapping;
import com.linecorp.bot.spring.boot.annotation.LineMessageHandler;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class DetectLangMessageHandler {
    public static final String INPUT_SPEC = "/detect_lang";
    public static final String DANDELION_LANGUAGE_DETECTION_ENDPOINT = "https://api.dandelion.eu/datatxt/li/v1/";
    private static final String DANDELION_AUTH_TOKEN = "";


    public static boolean isUrl(String string) {
        try {
            URL url = new URL(string);
            return true;
        } catch (MalformedURLException e) {
            return false;
        }
    }

    public static String generateEndpoint(String baseUrl, Map<String, String> queryStrings) {
        String endpoint = baseUrl;
        boolean first = true;
        for (String key : queryStrings.keySet()) {
            try {
                String encodedQueryString = URLEncoder.encode(queryStrings.get(key), "UTF-8");
                endpoint += (first ? "?" : "&") + key + "=" + encodedQueryString;
            } catch (Exception e) {
                continue;
            }
            if (first) {
                first = false;
            }
        }
        return endpoint;
    }

    public static String makeRequestToDandelion(Map<String, String> queryStrings)
            throws IOException {
        return "";
    }

    public static TextMessage replyMessage(String message) {
        String[] splittedMessage = message.split(" ");
        if (splittedMessage.length == 0) {
            throw new Error("Message should not be empty");
        } else if (!splittedMessage[0].equals(INPUT_SPEC)) {
            throw new Error("This method should only be executed with message beginning with"
                    + INPUT_SPEC);
        }

        String messageWithoutInputSpec = message.substring(splittedMessage[0].length());

        Map<String, String> queryStrings = new HashMap<String, String>();

        // Dandelion API Requirement, must have Auth token to send a request
        queryStrings.put("token", DANDELION_AUTH_TOKEN);

        return new TextMessage(messageWithoutInputSpec);
    }

}
